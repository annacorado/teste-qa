# Teste QA

# Desafio - Teste automatizado para Pacto Soluções.

## Descrição do Projeto

<p align="center"> Teste automatizado usando Cypress</p>

### Fluxo do cenário de teste: 
- Site usado para o desafio: http://automationpractice.com/index.php
- Specs necessárias: 
- [ ] Cadastro de uma nova conta
- [ ] Adicionar um produto ao carrinho.
- [ ] Concluir o processo da compra.
- [ ] Consultar o pedido feito.
- Organizar o código separando:
- [ ] Em etapas (Preparação, Ação e Validação)
- [ ] Em responsabilidades (Page Objects)
- [ ] Avaliar se é necessário usar ou cadastrar uma nova rota.
- Repositório do Github.
- GIF dos testes rodando (pode ser o vídeo gerado pelo Cypress).
- Descrição do desafio com as etapas feitas e detalhes para execução.
<br>
<br>


---
### Autora
Feito com 💜 por Anna Carolina 👋💻 

